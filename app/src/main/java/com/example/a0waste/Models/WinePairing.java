package com.example.a0waste.Models;

import java.util.ArrayList;

public class WinePairing {
    public ArrayList<String> pairedWines;
    public String pairingText;
    public ArrayList<ProductMatch> productMatches;
}
