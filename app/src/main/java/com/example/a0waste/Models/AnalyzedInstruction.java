package com.example.a0waste.Models;

import java.util.ArrayList;

public class AnalyzedInstruction {
    public String name;
    public ArrayList<Step> steps;
}
