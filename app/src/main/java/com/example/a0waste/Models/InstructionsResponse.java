package com.example.a0waste.Models;

import java.util.ArrayList;

public class InstructionsResponse {
    public String name;
    public ArrayList<Step> steps;
}
