package com.example.a0waste.Listeners;

public interface RecipeClickListener {
    void onRecipeClicked(String id);
}
